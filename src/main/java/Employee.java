import java.util.ArrayList;
import java.util.Arrays;

public class Employee
{
    //field
    private String name;
    private double salary;
    private int birthYear;
    private String[] kids;
    private String numbers;
    //    private final String products;

    //constructor
    public Employee(String n, double salary, int birthYear, String[] kids) {
        this.name = n;
        this.salary = salary;
        this.birthYear = birthYear;
        this.kids = kids;
        //this.products = "Cars";
    }
    public Employee(String name2)
    {
        this(name2,412,1452,new String[]{"abbas"});
    }
    public Employee(){}


    private void setDefault()
    {
        numbers = "1234";
    }


    //setter
    public void setName(String name) {
        if(name.length()<5)
        {
            System.err.println("This name is too short");
        }
        else
        {
            this.name = name;
        }
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }

    public void setKids(String[] kids) {
        this.kids = kids;
    }

    //getter
    public double getSalary() {
        return salary;
    }

    public String[] getKids() {
        return Arrays.copyOf(kids,kids.length);
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void eq(Employee emp2) {
        if(name.equals(emp2.name))
        {
            System.out.println("These objects are equal");
        }
        else
        {
            System.err.println("Not equal!");
        }
    }
    //string str = "" != null


}